define({ "api": [
  {
    "type": "GET",
    "url": "localhost/core/public/profile/fetchuserdata/userid/urlname",
    "title": "fetchuserdata",
    "group": "Profilepage",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>userId of the logged in user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "urlname",
            "description": "<p>urlName of the owner of the profile.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping the data of the profile owner and the friend status of the logged in person.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "data.userExist",
            "description": "<p>&quot;true&quot; if user is found &quot;false&quot; if not found.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "data.isOwner",
            "description": "<p>&quot;true&quot; if its the loggedin user profile and &quot;false&quot; for otherwise&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "data.sentPendingFriendRequest",
            "description": "<p>null for loggedin user but either &quot;true&quot; or &quot;false&quot; for other users.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "data.receivedPendingFriendRequest",
            "description": "<p>null for loggedin user but either &quot;true&quot; or &quot;false&quot; for other users.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.userprofile",
            "description": "<p>contains the user information e.g firstname, lastname, state, DOB.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"status\": \"true\",\n\t\"data\": {\n\t\t\"isOwner\": \"true\",\n\t\t\"userExist\": \"true\",\n\t\t\"arePals\": \"false\",\n\t\t\"momentsCount\": \"23\",\n\t\t\"palsCount\": \"3\",\n\t\t\"photosCount\": \"2\",\n\t\t\"videosCount\": \"1\",\n\t\t\"favouriteCount\": \"2\",\n\t\t\"sentPendingFriendRequest\": null,\n\t\t\"receivedPendingFriendRequest\": null,\n\t\t\"isBlocked\": false,\n\t\t\"userProfile\": {\n\t\t\t\"userId\": \"bf6ab5f3e55ac74ba38d77feac42ee85\",\n\t\t\t\"firstName\": \"Nsima\",\n\t\t\t\"lastName\": \"Edeheudim\",\n\t\t\t\"gender\": \"Male\",\n\t\t\t\"day\": \"10\",\n\t\t\t\"month\": \"8\",\n\t\t\t\"year\": \"1946\",\n\t\t\t\"birthday\": \"10-8-1946\",\n\t\t\t\"email\": \"nsimamfon@g.com\",\n\t\t\t\"phone\": \"07039201829\",\n\t\t\t\"settings\": null,\n\t\t\t\"onlineStatus\": null,\n\t\t\t\"accountStatus\": null,\n\t\t\t\"device\": null,\n\t\t\t\"username\": null,\n\t\t\t\"accountType\": null,\n\t\t\t\"dateJoined\": null,\n\t\t\t\"website\": \"nsima.com\",\n\t\t\t\"urlName\": \"nsima.edeheudim\",\n\t\t\t\"cityId\": \"18316\",\n\t\t\t\"stateId\": \"1727\",\n\t\t\t\"countryId\": \"NGA\",\n\t\t\t\"bioInfo\": \"assssssss\",\n\t\t\t\"city\": \"Calabar\",\n\t\t\t\"state\": \"Cross River\",\n\t\t\t\"country\": \"Nigeria\",\n\t\t\t\"userCoverMedia\": \"localhost\\/cuploader\\/public\\/uploads\\/videos\\/1493904995590b2e63195ead.jpg\",\n\t\t\t\"avatar\": \"localhost\\/cuploader\\/public\\/uploads\\/images\\/cropped\\/1493904995590b2e63195ea.jpg\",\n\t\t\t\"userCoverMediaType\": \"video\",\n\t\t\t\"isVerified\": null,\n\t\t\t\"registrationStep\": \"4\"\n\t\t}\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API fetches data from different tables in the database about a user, format into a json object and sends it to the client. The main tables used  are UserBiodata and UserAuthentication</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/profile/fetchbiodata.php",
    "groupTitle": "Profilepage",
    "name": "GetLocalhostCorePublicProfileFetchuserdataUseridUrlname"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/acceptpalrequest",
    "title": "acceptpalrequest",
    "name": "acceptpalrequest",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the logged in user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.palUserId",
            "description": "<p>userId userId of the pal sending the request.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping success information.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.status",
            "description": "<p>value will be sucess.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error:userNotLoggedId": [
          {
            "group": "Error:userNotLoggedId",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json with {&quot;status&quot;:&quot;false&quot;,&quot;message&quot;:&quot;user not logged in&quot;}.</p>"
          }
        ],
        "Error:userDoesnotExist": [
          {
            "group": "Error:userDoesnotExist",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json in the format {&quot;status&quot;:&quot;true&quot;,&quot;data&quot;:{&quot;status&quot;:&quot;failed&quot;}}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error:userNotLoggedId:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}.",
          "type": "Json"
        },
        {
          "title": "Error:userDoesnotExist:",
          "content": "{\"status\":\"true\",\"data\":{\"status\":\"failed\"}}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API accepts friend requests. It checks whether the userId and palUserId are in the database table, if no, it respone with the userDoesnotExist Error , if yes it checks whether the are pals already, if yes, it respone with a failed status, if no it inserts into pals table and updates palreuest table. If the user is not logged in, will respone with userNotLoggedId error</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/acceptpalrequest.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/addPalCategory",
    "title": "addPalCategory",
    "name": "addPalCategory",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User taking the action.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.categoryName",
            "description": "<p>The name of the category.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping the information of the created category.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API is used to create pals category. It inserts the categoryName , userId, date into the database.</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/addpalcategory.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/addPalToCategory",
    "title": "addPalToCategory",
    "name": "addPalToCategory",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the logged in user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.categoryId",
            "description": "<p>categoryId of the category involved.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.palId",
            "description": "<p>userId userId of the pal that will be added.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping junctionDetails and categoryDetails (both object).</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.junctionDetails",
            "description": "<p>object containig the userId, palId and userCategoryId.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.categoryDetails",
            "description": "<p>object containig the userId, categoryTitle, createdDate.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error:userNotLoggedId": [
          {
            "group": "Error:userNotLoggedId",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json with {&quot;status&quot;:&quot;false&quot;,&quot;message&quot;:&quot;user not logged in&quot;}.</p>"
          }
        ],
        "Error:userDoesnotExist": [
          {
            "group": "Error:userDoesnotExist",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json in the format {&quot;status&quot;:&quot;true&quot;,&quot;data&quot;:{&quot;status&quot;:&quot;failed&quot;}}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error:userNotLoggedId:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}.",
          "type": "Json"
        },
        {
          "title": "Error:userDoesnotExist:",
          "content": "{\"status\":\"true\",\"data\":{\"status\":\"failed\"}}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API adds a pal to pal category. It checks whether the userId and palId are in the database table, if no, it respone with the userDoesnotExist Error , if yes it checks whether the pals has been added, if yes, it respone with a failed status, if no,  it inserts into the palcategory table.  If the user is not logged in, will respone with userNotLoggedId error</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/addpaltocategory.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/blockpal",
    "title": "blockpal",
    "name": "blockpal",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User taking the action.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.palUserId",
            "description": "<p>userId of the user that will be blocked</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping the success information {&quot;status&quot;:&quot;true&quot;,&quot;data&quot;:{&quot;status&quot;:&quot;success&quot;}}</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API is used to block pal. it updates friendstatus field in userpals table to 2</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/blockpal.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/addPalCategory",
    "title": "createandadd",
    "name": "createandadd",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User taking the action.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.palId",
            "description": "<p>userId of the User that will be added to the category.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.categoryName",
            "description": "<p>The name of the category.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.junctionDetails",
            "description": "<p>object containig the userId, palId and userCategoryId.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.categoryDetails",
            "description": "<p>object containig the userId, categoryTitle, createdDate.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error:userNotLoggedId": [
          {
            "group": "Error:userNotLoggedId",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json with {&quot;status&quot;:&quot;false&quot;,&quot;message&quot;:&quot;user not logged in&quot;}.</p>"
          }
        ],
        "Error:userDoesnotExist": [
          {
            "group": "Error:userDoesnotExist",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json in the format {&quot;status&quot;:&quot;true&quot;,&quot;data&quot;:{&quot;status&quot;:&quot;failed&quot;}}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error:userNotLoggedId:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}.",
          "type": "Json"
        },
        {
          "title": "Error:userDoesnotExist:",
          "content": "{\"status\":\"true\",\"data\":{\"status\":\"failed\"}}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API creates a palcategory and immediately adds a pal to the category . It checks whether the userId and palId are in the database table, if no, it respone with the userDoesnotExist Error , If the user is not logged in, will respone with userNotLoggedId error.</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/createandadd.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/declinepalrequest",
    "title": "declinepalrequest",
    "name": "declinepalrequest",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User taking the action.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.palUserId",
            "description": "<p>userId of the user that will be blocked</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping the success information {&quot;status&quot;:&quot;true&quot;,&quot;data&quot;:{&quot;status&quot;:&quot;success&quot;}}</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API is used to decline palrequest. It delete the row in the palrequest where you have the two users</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/declinepalrequest.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/disconnect",
    "title": "disconnect",
    "name": "disconnect",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User in question.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.palUserId",
            "description": "<p>userId of the pal that will be disconnected.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping success information in. {&quot;status&quot;:&quot;true&quot;,&quot;data&quot;:{&quot;status&quot;:&quot;success&quot;}}</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.user",
            "description": "<p>contains the user information e.g firstname, lastname, state, DOB.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Responses:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API fetches unfriends a pal from a user. it deletes the row that has the user and palUserId information from both the useral table and palrequest table</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/disconnect.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/editProfile",
    "title": "editprofile",
    "name": "editprofile",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping several  information.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the loggedin user.</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data.profileData",
            "description": "<p>object that contains phone,website,email,location, birthday, bio.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.profileData.profile-bio",
            "description": "<p>string that has bio info of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.profileData.profile-contact-numbers",
            "description": "<p>string that has the phone number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.profileData.profile-website",
            "description": "<p>string that contains website.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.profileData.profile-email",
            "description": "<p>string that has the email of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data.profileData.profile-location",
            "description": "<p>object  that contains country, state and city codes.</p>"
          },
          {
            "group": "Parameter",
            "type": "sting",
            "optional": false,
            "field": "data.profileData.profile-location.city",
            "description": "<p>contains  city code of user.</p>"
          },
          {
            "group": "Parameter",
            "type": "sting",
            "optional": false,
            "field": "data.profileData.profile-location.state",
            "description": "<p>contains  state code of user.</p>"
          },
          {
            "group": "Parameter",
            "type": "sting",
            "optional": false,
            "field": "data.profileData.profile-location.country",
            "description": "<p>contains  country code of user.</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data.profileData.profile-birthday",
            "description": "<p>object  that contains day, month, year of birth of user.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "data.profileData.profile-birthday.day",
            "description": "<p>contains day of birth of  user.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "data.profileData.profile-birthday.month",
            "description": "<p>contains month of birth of  user.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "data.profileData.profile-birthday.year",
            "description": "<p>contains year of birth of  user.</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data.bioDataPrivacy",
            "description": "<p>objects that contains bioData Privacy options</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data.locationPrivacy",
            "description": "<p>object that contains location Privacy.</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data.birthdayPrivacy",
            "description": "<p>object that contains birthday Privacy.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value true.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>in the format &quot;data&quot;:{&quot;success&quot;:&quot;true&quot;},&quot;message&quot;:&quot;information updated&quot;}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "success response:",
          "content": "{\"status\":\"true\",\"data\":{\"success\":\"true\"},\"message\":\"information updated\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>will be like {&quot;status&quot;:&quot;true&quot;,&quot;data&quot;:{&quot;success&quot;:&quot;false&quot;},&quot;message&quot;:&quot;information not updated&quot;}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"true\",\"data\":{\"success\":\"false\"},\"message\":\"information not updated\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API updates user information in UserBiodata table</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/editprofile.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/fetchpalrequest",
    "title": "fetchpalrequest",
    "name": "fetchpalrequest",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User in question.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "data.pageStart",
            "description": "<p>pagination start value, usually null for first time.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "data.pageEnd",
            "description": "<p>pagination last retrieved value, usually null for first time.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping objects that holds friend request and accepts information as shown in sucess example, .</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pageEnd",
            "description": "<p>This is the value of the smallet  Id that is returned in an array.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "pageStart",
            "description": "<p>This is the value of the biggest Id that is returned in an array.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API fetches both accepted pal request sent to others and recieved palrequest.</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/fetchpalrequest.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/user/loadProfile",
    "title": "loadprofile",
    "name": "loadProfile",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User in question.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping user information.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.user",
            "description": "<p>contains the user information e.g firstname, lastname, state, DOB.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API fetches data from different tables in the database about a user, format into a json object and sends it to the client. The main tables used  are UserBiodata and UserAuthentication</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/user/loadprofile.php",
    "groupTitle": "User"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/moment/deletecomment",
    "title": "deletecomment",
    "name": "deletecomment",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.commentId",
            "description": "<p>commentId of the comment that will be deleted.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping success information. in the format &quot;data&quot;:{&quot;status&quot;:&quot;success&quot;}}</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API  will be used to fake deleting of comment by updating the trashed value to 1 so that when comments are fetched, the comment with the commentid will not be fetched</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/deletecomment.php",
    "groupTitle": "moment"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/moment/deletemoment",
    "title": "deletemoment",
    "name": "deletemoment",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.momentId",
            "description": "<p>momentId of the moment that will be deleted.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping success information. in the format &quot;data&quot;:{&quot;status&quot;:&quot;success&quot;}}</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API  will be used to fake deleting of moment by updating the trashed value to 1 in moment table so that when moments are fetched, the moment with the momentid will not be fetched</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/deletemoment.php",
    "groupTitle": "moment"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/moment/edit",
    "title": "edit",
    "name": "edit",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the logged in user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.text",
            "description": "<p>the write up (text) in the moment.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.momentId",
            "description": "<p>the momentId of the moment that will be edited.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "data.momentType",
            "description": "<p>the momentType of the moment that will be edited ( a hash that will diffentiate text, video, photo, livestream).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping the moment that has been edited.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"status\": \"true\",\n\t\"data\": {\n\t\t\"moment\": {\n\t\t\t\"momentId\": \"590359708365e9.35403862uijk8was6\",\n\t\t\t\"content\": \"tell them that nodejs is hot\",\n\t\t\t\"media\": null,\n\t\t\t\"privacy\": \"1\",\n\t\t\t\"time\": \"1493391728\",\n\t\t\t\"userId\": \"bf6ab5f3e55ac74ba38d77feac42ee85\",\n\t\t\t\"isOwner\": true,\n\t\t\t\"momentType\": \"1\",\n\t\t\t\"commentCount\": \"0\",\n\t\t\t\"originalMomentId\": \"\",\n\t\t\t\"hasRebroadcasted\": \"false\",\n\t\t\t\"rebroadcastCount\": \"0\",\n\t\t\t\"isNotificationEnabled\": \"false\",\n\t\t\t\"isNotificationEnabled\": \"false\",\n\t\t\t\"hasRebroadcasted\": \"false\",\n\t\t\t\t\"device\": \"1\",\n\t\t\t\t\"username\": \"07039201829\",\n\t\t\t\t\"accountType\": null,\n\t\t\t\t\"dateJoined\": \"2016-10-20 16:58:36\",\n\t\t\t\"website\": \"nsima.com\",\n\t\t\t\t\"urlName\": \"NsimaEdeheudim\",\n\t\t\t\t\"cityId\": \"18316\",\n\t\t\t\t\"stateId\": \"1727\",\n\t\t\t\t\"countryId\": \"NGA\",\n\t\t\t\t\"bioInfo\": \"assssssss\",\n\t\t\t\t\"city\": \"Calabar\",\n\t\t\t\t\"state\": \"Cross River\",\n\t\t\t\t\"country\": \"Nigeria\",\n\t\t\t\t\"userCoverMedia\": null,\n\t\t\t\t\"avatar\": \"localhost\\/cuploader\\/public\\/uploads\\/images\\/cropped\\/149061767658d9054ce7465.jpg\",\n\t\t\t\t\"userCoverMediaType\": null\n\t\t\t},\n\t\t\t\"commentObject\": {\n\t\t\t\t\"list\": [],\n\t\t\t\t\"commentsReturned\": 0,\n\t\t\t\t\"pageStart\": null,\n\t\t\t\t\"pageEnd\": null\n\t\t\t},\n\t\t\t\"commentersCount\": \"0\"\n\t\t}\n}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>type 1  user not logged in.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response type 1:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}.",
          "type": "json"
        },
        {
          "title": "Error-Response type 2:",
          "content": "{\"status\":\"true\",\"data\":{\"success\":\"false\"},\"message\":\"could not update database\"}.",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API  will be used to fake deleting of comment by updating the trashed value to 1 so that when comments are fetched, the comment with the commentid will not be fetched</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/edit.php",
    "groupTitle": "moment"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/moment/favourite",
    "title": "favourite",
    "name": "favourite_",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User that is taking the action.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.momentId",
            "description": "<p>momentId of the moment that will be favourited or unfavourited.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.action",
            "description": "<p>contains &quot;favourite&quot; or &quot;unfavourite&quot; depending on what the user wants to do.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping success information.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.status",
            "description": "<p>value will be sucess.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"status\":\"true\",\"data\":{\"status\":\"success}}.",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error:userNotLoggedId": [
          {
            "group": "Error:userNotLoggedId",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json with {&quot;status&quot;:&quot;false&quot;,&quot;message&quot;:&quot;user not logged in&quot;}.</p>"
          }
        ],
        "Error:FailedToUpdate": [
          {
            "group": "Error:FailedToUpdate",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json in the format {&quot;status&quot;:&quot;true&quot;,&quot;data&quot;:{&quot;status&quot;:&quot;failed&quot;}}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error:userNotLoggedId:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}.",
          "type": "Json"
        },
        {
          "title": "Error:FailedToUpdate:",
          "content": "{\"status\":\"true\",\"data\":{\"status\":\"failed\"}}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API is used to favourite a moment, if the action is favourite, the moment will be favourited by inserting the userId and MomentId into favourite table. if the action is unfavourite, the the row holding the data will be deleted.</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/favourite.php",
    "groupTitle": "moment"
  },
  {
    "type": "GET",
    "url": "localhost/core/public/moment/fetchcommenters/userId/momentId/pageStart/pageEnd",
    "title": "fetchcommenters",
    "name": "fetchcommenters",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>object wrapping information from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the logged in user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.momentId",
            "description": "<p>momentId of the moment.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "data.pageStart",
            "description": "<p>For the first load, this value will be null.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "data.pageEnd",
            "description": "<p>For the first load, this value will be null but will increase when the first might have been pulled. its used for pagination</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping data fetched and arranged.</p>"
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data.details",
            "description": "<p>contains the pageEnd, pageStart and the users object that commented on the moment.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " {\n\"status\": \"true\",\n\t\"data\": {\n\t\"details\": [{\n\t\t\"time\": \"1492764355\",\n\t\t\t\"arePals\": \"true\",\n\t\t\"userObject\": {\n\t\t\t   \"userId\": \"924f06a73148df910dadd86ffe62ead4\",\n\t\t\t   \"firstName\": \"Ifiok \",\n\t\t\t   \"lastName\": \"Okon\",\n\t\t\t\t \"gender\": \"Male\",\n\t\t\t\t\"day\": \"5\",\n\t\t\t\t\"month\": \"6\",\n\t\t\t\t\"year\": \"1955\",\n\t\t\t\t\"birthday\": \"5-6-1955\",\n\t\t\t\t\"email\": \"\",\n\t\t\t   \"phone\": \"09055109775\",\n\t\t\t\t\"settings\": null,\n\t\t\t\t\"onlineStatus\": null,\n\t\t\t   \"accountStatus\": \"1\",\n\t\t\t\t\"device\": \"1\",\n\t\t\t\t\"username\": \"09055109775\",\n\t\t\t\t\"accountType\": null,\n\t\t\t\t\"dateJoined\": \"2016-11-01 16:42:41\",\n\t\t\t\t\"website\": null,\n\t\t\t\t\"urlName\": \"ifiok okon\",\n\t\t\t\t\"cityId\": \"9734\",\n\t\t\t\t\"stateId\": \"1461\",\n\t\t\t\t\"countryId\": \"AND\",\n\t\t\t\t\"bioInfo\": null,\n\t\t\t\t\"city\": \"Les Escaldes\",\n\t\t\t\t\"state\": \"Escaldes-Engordany\",\n\t\t\t\t\"country\": \"Andorra\",\n\t\t\t\t\"userCoverMedia\": null,\n\t\t\t\t\"avatar\": \"localhost\\/cuploader\\/public\\/uploads\\/images\\/cropped\\/14780153705818b98a47d53.png\",\n\t\t\t\t\"userCoverMediaType\": null\n\t\t\t}\n\t\t}, {\n\t\t\t\"time\": \"1492764230\",\n\t\t\t\"arePals\": \"false\",\n\t\t\t\"userObject\": {\n\t\t\t\t\"userId\": \"bf6ab5f3e55ac74ba38d77feac42ee85\",\n\t\t\t\t\"firstName\": \"Nsima\",\n\t\t\t\t\"lastName\": \"Edeheudim\",\n\t\t\t\t\"gender\": \"Male\",\n\t\t\t\t\"day\": \"10\",\n\t\t\t\t\"month\": \"8\",\n\t\t\t\t\"year\": \"1946\",\n\t\t\t\t\"birthday\": \"10-8-1946\",\n\t\t\t\t\"email\": \"nsimamfon@g.com\",\n\t\t\t\t\"phone\": \"07039201829\",\n\t\t\t\t\"settings\": null,\n\t\t\t\t\"onlineStatus\": null,\n\t\t\t\t\"accountStatus\": \"1\",\n\t\t\t\t\"device\": \"1\",\n\t\t\t\t\"username\": \"07039201829\",\n\t\t\t\t\"accountType\": null,\n\t\t\t\t\"dateJoined\": \"2016-10-20 16:58:36\",\n\t\t\t\t\"website\": \"nsima.com\",\n\t\t\t\t\"urlName\": \"NsimaEdeheudim\",\n\t\t\t\t\"cityId\": \"18316\",\n\t\t\t\t\"stateId\": \"1727\",\n\t\t\t\t\"countryId\": \"NGA\",\n\t\t\t\t\"bioInfo\": \"assssssss\",\n\t\t\t\t\"city\": \"Calabar\",\n\t\t\t\t\"state\": \"Cross River\",\n\t\t\t\t\"country\": \"Nigeria\",\n\t\t\t\t\"userCoverMedia\": null,\n\t\t\t\t\"avatar\": \"localhost\\/cuploader\\/public\\/uploads\\/images\\/cropped\\/149061767658d9054ce7465.jpg\",\n\t\t\t\t\"userCoverMediaType\": null\n\t\t\t}\n\t\t}],\n\t\t\"count\": 2,\n\t\t\"pageStart\": \"1492764355\",\n\t\t\"pageEnd\": \"1492764230\"\n\t}\n}.",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error:userNotLoggedId": [
          {
            "group": "Error:userNotLoggedId",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json with {&quot;status&quot;:&quot;false&quot;,&quot;message&quot;:&quot;user not logged in&quot;}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error:userNotLoggedId:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}.",
          "type": "Json"
        }
      ]
    },
    "description": "<p>This API  fetches list of users that commented on a moment. It returns the object of the users and pageEnd with pageStart used for pagination</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/fetchcommenters.php",
    "groupTitle": "moment"
  },
  {
    "type": "GET",
    "url": "localhost/core/public/moment/fetchcommenters/userId/commentId/pageStart/pageEnd",
    "title": "fetchcommentreplies",
    "name": "fetchcommentreplies",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>object wrapping information from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the logged in user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.commentId",
            "description": "<p>commentId of the comment.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "data.pageStart",
            "description": "<p>For the first load, this value will be null.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "data.pageEnd",
            "description": "<p>For the first load, this value will be null but will increase when the first might have been pulled. its used for pagination</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping data fetched and arranged as shown in apiSuccessExample.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " {\n\t\"status\": \"true\",\n\t\"data\": {\n\t\t\"commentsReturned\": 1,\n\t\t\"pageStart\": \"32\",\n\t\t\"pageEnd\": \"32\",\n\t\t\"details\": [{\n\t\t\t\"commentId\": \"59049ed6539d98.1021179359qkyg8ez\",\n\t\t\t\"momentId\": \"59049ec786a896.40130797iy14jg6a7\",\n\t\t\t\"content\": \"i am done\",\n\t\t\t\"time\": \"1493475030\",\n\t\t\t\"userId\": \"bf6ab5f3e55ac74ba38d77feac42ee85\",\n\t\t\t\"trashed\": \"0\",\n\t\t\t\"userInfo\": {\n\t\t\t\t\"userId\": \"bf6ab5f3e55ac74ba38d77feac42ee85\",\n\t\t\t\t\"firstName\": \"Nsima\",\n\t\t\t\t\"lastName\": \"Edeheudim\",\n\t\t\t\t\"gender\": \"Male\",\n\t\t\t\t\"day\": \"10\",\n\t\t\t\t\"month\": \"8\",\n\t\t\t\t\"year\": \"1946\",\n\t\t\t\t\"birthday\": \"10-8-1946\",\n\t\t\t\t\"email\": \"nsimamfon@g.com\",\n\t\t\t\t\"phone\": \"07039201829\",\n\t\t\t\t\"settings\": null,\n\t\t\t\t\"onlineStatus\": null,\n\t\t\t\t\"accountStatus\": \"1\",\n\t\t\t\t\"device\": \"1\",\n\t\t\t\t\"username\": \"07039201829\",\n\t\t\t\t\"accountType\": null,\n\t\t\t\t\"dateJoined\": \"2016-10-20 16:58:36\",\n\t\t\t\t\"website\": \"nsima.com\",\n\t\t\t\t\"urlName\": \"NsimaEdeheudim\",\n\t\t\t\t\"cityId\": \"18316\",\n\t\t\t\t\"stateId\": \"1727\",\n\t\t\t\t\"countryId\": \"NGA\",\n\t\t\t\t\"bioInfo\": \"assssssss\",\n\t\t\t\t\"city\": \"Calabar\",\n\t\t\t\t\"state\": \"Cross River\",\n\t\t\t\t\"country\": \"Nigeria\",\n\t\t\t\t\"userCoverMedia\": null,\n\t\t\t\t\"avatar\": \"localhost\\/cuploader\\/public\\/uploads\\/images\\/cropped\\/149061767658d9054ce7465.jpg\",\n\t\t\t\t\"userCoverMediaType\": null\n\t\t\t},\n\t\t\t\"media\": null,\n\t\t\t\"replyCount\": \"0\",\n\t\t\t\"mediaId\": null\n\t\t}]\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error:userNotLoggedId": [
          {
            "group": "Error:userNotLoggedId",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json with {&quot;status&quot;:&quot;false&quot;,&quot;message&quot;:&quot;user not logged in&quot;}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error:userNotLoggedId:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}.",
          "type": "Json"
        }
      ]
    },
    "description": "<p>This API  fetches comment replies to another comment. It returns the details together with the  pageEnd with pageStart used for pagination</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/fetchcommentreplies.php",
    "groupTitle": "moment"
  },
  {
    "type": "GET",
    "url": "localhost/core/public/moment/fetchcomments/userId/momentId/pageStart/pageEnd",
    "title": "fetchcomments",
    "name": "fetchcomments",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>userId of the logged in user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "momentId",
            "description": "<p>momentId of the moment.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageStart",
            "description": "<p>For the first load, this value will be null.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageEnd",
            "description": "<p>For the first load, this value will be null but will increase when the first might have been pulled. its used for pagination</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": "<p>wrapping data fetched and arranged as shown in apiSuccessExample.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\n{\n\t\"status\": \"true\",\n\t\"data\": {\n\t\"commentsReturned\": 1,\n\t\"pageStart\": \"33\",\n\t\"pageEnd\": \"33\",\n\t\"details\": [{\n\t\t\"commentId\": \"5904a419992277.61815585zxtaeq9b1\",\n\t\t\"momentId\": \"5904a40fbcb676.30363140jxh2479d8\",\n\t\t\"content\": \"one comment\",\n\t\t\"time\": \"1493476377\",\n\t\t\"userId\": \"bf6ab5f3e55ac74ba38d77feac42ee85\",\n\t\t\"trashed\": \"0\",\n\t\t\"userInfo\": {\n\t\t\t\"userId\": \"bf6ab5f3e55ac74ba38d77feac42ee85\",\n\t\t\t\"firstName\": \"Nsima\",\n\t\t\t\"lastName\": \"Edeheudim\",\n\t\t\t\"gender\": \"Male\",\n\t\t\t\"day\": \"10\",\n\t\t\t\"month\": \"8\",\n\t\t\t\"year\": \"1946\",\n\t\t\t\"birthday\": \"10-8-1946\",\n\t\t\t\"email\": \"nsimamfon@g.com\",\n\t\t\t\"phone\": \"07039201829\",\n\t\t\t\"settings\": null,\n\t\t\t\"onlineStatus\": null,\n\t\t\t\"accountStatus\": \"1\",\n\t\t\t\"device\": \"1\",\n\t\t\t\"username\": \"07039201829\",\n\t\t\t\"accountType\": null,\n\t\t\t\"dateJoined\": \"2016-10-20 16:58:36\",\n\t\t\t\"website\": \"nsima.com\",\n\t\t\t\"urlName\": \"NsimaEdeheudim\",\n\t\t\t\"cityId\": \"18316\",\n\t\t\t\"stateId\": \"1727\",\n\t\t\t\"countryId\": \"NGA\",\n\t\t\t\"bioInfo\": \"assssssss\",\n\t\t\t\"city\": \"Calabar\",\n\t\t\t\"state\": \"Cross River\",\n\t\t\t\"country\": \"Nigeria\",\n\t\t\t\"userCoverMedia\": null,\n\t\t\t\"avatar\": \"localhost\\/cuploader\\/public\\/uploads\\/images\\/cropped\\/149061767658d9054ce7465.jpg\",\n\t\t\t\"userCoverMediaType\": null\n\t\t},\n\t\t\"media\": null,\n\t\t\"replyCount\": \"0\",\n\t\t\"mediaId\": null\n\t}]\n}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error:userNotLoggedId": [
          {
            "group": "Error:userNotLoggedId",
            "type": "Json",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>This will be a json with {&quot;status&quot;:&quot;false&quot;,&quot;message&quot;:&quot;user not logged in&quot;}.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error:userNotLoggedId:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}.",
          "type": "Json"
        }
      ]
    },
    "description": "<p>This API  fetches comments for a moment . It returns the details together with the  pageEnd with pageStart used for pagination</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/fetchcomments.php",
    "groupTitle": "moment"
  },
  {
    "type": "GET",
    "url": "localhost/core/public/moment/hidecomment/userid/commentid",
    "title": "hidecomment",
    "name": "hidecomment",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>userId of the User in question.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "commentId",
            "description": "<p>commentId of the comment that will be hidden.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>in  the format &quot;data&quot;:{&quot;status&quot;:&quot;success}</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"status\":\"true\",\"data\":{\"status\":\"success}}",
          "type": "Json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API  will be used to fake hiding of comment by insering it into usercommenthide  so that when comments are fetched, the comment with the commentid will not be fetched for the logged in user</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/hidecomment.php",
    "groupTitle": "moment"
  },
  {
    "type": "POST",
    "url": "localhost/core/public/moment/hidemoment",
    "title": "hidemoment",
    "name": "hidemoment",
    "group": "moment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>wraps the data sent from client.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.userId",
            "description": "<p>userId of the User in question.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data.momentId",
            "description": "<p>momentId of the moment that will be hidden.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>value will be true for user logged in.</p>"
          },
          {
            "group": "Success 200",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>in  the format &quot;data&quot;:{&quot;status&quot;:&quot;success}</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\"status\":\"true\",\"data\":{\"status\":\"success}}",
          "type": "Json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "object",
            "optional": false,
            "field": "Error-Response",
            "description": "<p>user not logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\"status\":\"false\",\"message\":\"user not logged in\"}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This API  will be used to fake hiding of moment by insering it into a table  so that when moments are fetched, the moment with the momentid will not be fetched for the logged in user</p>",
    "version": "0.0.0",
    "filename": "core/application/controllers/moment/hidemoment.php",
    "groupTitle": "moment"
  }
] });
